const { v4 } = require('uuid');

const mailer = require('./mail');

async function confirmEmailLink(url, userId, redis) {
  const uuid = v4();

  await redis.set(uuid, userId, 'ex', 60 * 60 * 24);

  return `${url}/confirm/${uuid}`;
}

function sendConfirmationEmail(recipient, link) {
  const content = `<a href="${link}">confirm email</a>`;
  return mailer.send(recipient, 'Confirm Email', content);
}

module.exports = {
  confirmEmailLink,
  sendConfirmationEmail
};
