// --------------------------------------------------------------------------
// QUERY & MUTATION
// --------------------------------------------------------------------------
const usersQuery = /* GraphQL */ `
  query users($first: Int) {
    users(first: $first) {
      aggregate {
        count
      }
      pageInfo {
        hasNextPage
      }
      edges {
        cursor
        node {
          id
          email
          firstname
          lastname
          phone
          role
          createdAt
          updatedAt
        }
      }
    }
  }
`;

const createUserMutation = /* GraphQL */ `
  mutation createUser($input: UserCreateInput!) {
    createUser(input: $input) {
      id
      email
      firstname
      lastname
      phone
      role
    }
  }
`;

const signupMutation = /* GraphQL */ `
  mutation signupMutation($input: UserSignupInput!) {
    signup(input: $input) {
      user {
        id
        email
        firstname
        lastname
        phone
        role
      }
      token
    }
  }
`;

const loginMutation = /* GraphQL */ `
  mutation loginMutation($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      user {
        id
        email
        lastname
        firstname
        phone
        role
      }
      token
    }
  }
`;

const sendConfirmationEmailMutation = /* GraphQL */ `
  mutation sendConfirmationEmailMutation($userId: String!) {
    sendConfirmationEmail(userId: $userId)
  }
`;

// --------------------------------------------------------------------------
// HELPERS
// --------------------------------------------------------------------------
function testUser(userRes, userExpected) {
  expect(userRes).toBeDefined();
  expect(userRes.firstname).toEqual(userExpected.firstname);
  expect(userRes.lastname).toEqual(userExpected.lastname);
  expect(userRes.email).toEqual(userExpected.email);
  expect(userRes.phone).toEqual(userExpected.phone);
  expect(userRes.role).toEqual(userExpected.role);
  expect(userRes.password).toBeUndefined();
}

// --------------------------------------------------------------------------
// TESTS
// --------------------------------------------------------------------------
describe('> Users', () => {
  let server;
  let redis;
  let user;

  beforeAll(async () => {
    const {
      server: currentServer,
      redis: currentRedis
    } = await global.createServer();
    server = currentServer;
    redis = currentRedis;
  });

  describe('query', () => {
    test('users', async () => {
      await server
        .sendQL(usersQuery, { first: 5 })
        .expect(({ body }) => {
          expect(body.data.users).toBeDefined();
          expect(body.data.users.aggregate.count).toBeDefined();
          expect(body.data.users.pageInfo.hasNextPage).toBe(true);
          expect(body.data.users.edges).toBeInstanceOf(Array);
          expect(body.data.users.edges.length).toBe(5);
          expect(body.data.users.edges[0].cursor).toBeDefined();
          expect(body.data.users.edges[0].node).toBeInstanceOf(Object);
        })
        .expect(200);
    });

    test('users - books', async () => {
      await server
        .sendQL(usersQuery, { first: 5 })
        .expect(({ body }) => {
          expect(body.data.users).toBeDefined();
          expect(body.data.users.aggregate.count).toBeDefined();
          expect(body.data.users.pageInfo.hasNextPage).toBe(true);
          expect(body.data.users.edges).toBeInstanceOf(Array);
          expect(body.data.users.edges.length).toBe(5);
          expect(body.data.users.edges[0].cursor).toBeDefined();
          expect(body.data.users.edges[0].node).toBeInstanceOf(Object);
        })
        .expect(200);
    });
  });

  describe('mutation', () => {
    test('createUser (admin-only)', async () => {
      const createUser = global.createUser();

      const adminToken = global.createToken({
        email: 'hello@jonathanrobic.fr',
        role: 'ADMIN'
      });

      await server
        .sendQL(createUserMutation, { input: createUser }, adminToken)
        .expect(({ body }) => {
          testUser(body.data.createUser, createUser);
        })
        .expect(200);
    });

    test('signup', async () => {
      const createUser = global.createUser();

      await server
        .sendQL(signupMutation, { input: createUser })
        .expect(({ body }) => {
          testUser(body.data.signup.user, createUser);
          expect(body.data.signup.token).toEqual(expect.any(String));
          user = body.data.signup.user; // eslint-disable-line
        })
        .expect(200);
    });

    test('signup - email already exists', async () => {
      const createUser = global.createUser({ email: user.email });

      await server
        .sendQL(signupMutation, { input: createUser })
        .expect(({ body }) => {
          expect(global.getErrorsMessage(body.errors)).toContain(
            'USER_ALREADY_EXISTS'
          );
        })
        .expect(200);
    });

    test('login', async () => {
      await server
        .sendQL(loginMutation, { email: user.email, password: 'secret42' })
        .expect(({ body }) => {
          testUser(body.data.login.user, user);
          expect(body.data.login.token).toEqual(expect.any(String));
        })
        .expect(200);
    });

    test('login - user not found', async () => {
      await server
        .sendQL(loginMutation, {
          email: 'test909999@test.fr',
          password: 'secret43'
        })
        .expect(({ body }) => {
          expect(global.getErrorsMessage(body.errors)).toContain(
            'USER_NOT_FOUND'
          );
        })
        .expect(200);
    });

    test('login - invalid password', async () => {
      await server
        .sendQL(loginMutation, { email: user.email, password: 'secret43' })
        .expect(({ body }) => {
          expect(global.getErrorsMessage(body.errors)).toContain(
            'INVALID_PASSWORD'
          );
        })
        .expect(200);
    });

    test('confirmation link', async () => {
      let confirmationLink;
      await server
        .sendQL(sendConfirmationEmailMutation, { userId: user.id })
        .expect(({ body }) => {
          expect(body.data.sendConfirmationEmail).toBeDefined();
          confirmationLink = body.data.sendConfirmationEmail;
        })
        .expect(200);

      await server.get(confirmationLink).expect(204);

      const uuid = confirmationLink.split('/').reverse()[0];
      const value = await redis.get(uuid);
      expect(value).toBeNull();
    });
  });
});
