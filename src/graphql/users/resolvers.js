const bcrypt = require('bcryptjs');

const { createToken } = require('../../utils/auth');
const {
  confirmEmailLink,
  sendConfirmationEmail
} = require('../../utils/confirmEmail');

module.exports = {
  Query: {
    async users(root, args, context) {
      const users = await context.prisma.usersConnection({
        ...args
      });

      const count = await context.prisma
        .usersConnection()
        .aggregate()
        .count();

      return {
        ...users,
        aggregate: {
          count
        }
      };
    }
  },
  Mutation: {
    createUser(root, args, context) {
      return context.prisma.createUser(args.input);
    },
    signup: async (root, args, context) => {
      const password = await bcrypt.hash(args.input.password, 10);

      const userExists = await context.prisma.$exists.user({
        email: args.input.email
      });

      if (userExists) {
        throw new Error('USER_ALREADY_EXISTS');
      }

      const user = await context.prisma.createUser({ ...args.input, password });

      const token = createToken({
        user: { id: user.id, email: user.email, role: user.role }
      });

      return {
        token,
        user
      };
    },
    login: async (root, args, context) => {
      const user = await context.prisma.user({ email: args.email });

      if (!user) {
        throw new Error('USER_NOT_FOUND');
      }

      const valid = await bcrypt.compare(args.password, user.password);

      if (!valid) {
        throw new Error('INVALID_PASSWORD');
      }

      const token = createToken({ userId: user.id });

      return {
        token,
        user
      };
    },
    sendConfirmationEmail: async (root, args, context) => {
      const user = await context.prisma.user({ id: args.userId });

      if (!user) {
        throw new Error('USER_NOT_FOUND');
      }

      const link = await confirmEmailLink(context.url, user.id, context.redis);

      await sendConfirmationEmail(user.email, link);

      return link;
    }
  },
  User: {
    books(root, args, context) {
      return context.prisma
        .user({
          id: root.id
        })
        .books();
    }
  }
};
