require('../config/env');
// require('dotenv').config();

const { GraphQLServer } = require('graphql-yoga');
const path = require('path');
const { fileLoader, mergeTypes } = require('merge-graphql-schemas');
const Redis = require('ioredis');

const { prisma } = require('./generated/prisma-client');
const { name, version } = require('../package.json');
const { permissions } = require('./graphql/permissions');
const { getUser } = require('./utils/auth');

// Types
const typesArray = fileLoader(path.join(__dirname, './graphql/**/*.graphql'));
const typesMerged = mergeTypes(typesArray);
const resolvers = fileLoader(path.join(__dirname, './graphql/**/resolvers.js'));

const startServer = async () => {
  const redis = new Redis(process.env.REDIS_URL);

  const server = new GraphQLServer({
    typeDefs: typesMerged,
    resolvers,
    middlewares: [permissions],
    context: req => ({
      ...req,
      url:
        process.env.NODE_ENV === 'test'
          ? ''
          : `${req.request.protocol}://${req.request.get('host')}`,
      prisma,
      redis,
      auth: getUser(req)
    })
  });

  server.express
    .get('/version', (_, res) => {
      res.send({ name, version });
    })
    .get('/confirm/:uuid', async (req, res) => {
      const { uuid } = req.params;
      const userId = await redis.get(uuid);

      if (!userId) {
        throw new Error('EMAIL_CONFIRM_USER_INVALID');
      }
      await redis.del(uuid);

      await prisma.updateUser({
        data: { confirmed: true },
        where: { id: userId }
      });

      res.sendStatus(204);
    });

  const PORT = process.env.NODE_ENV === 'test' ? 0 : '4000';

  const app = await server.start({ port: PORT });

  return {
    app,
    prisma,
    redis
  };
};

module.exports = startServer;
