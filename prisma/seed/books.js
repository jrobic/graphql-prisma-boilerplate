const faker = require('faker');
const times = require('ramda/src/times');
const { prisma } = require('../../src/generated/prisma-client');

faker.seed(2019);

function createBook({ id }) {
  const book = {
    title: faker.name.title(),
    author: {
      connect: { id }
    }
  };

  return prisma.createBook(book);
}

function createBooksPerUser(user) {
  return Promise.all(times(() => createBook(user), 20));
}

function createBooks(users) {
  return Promise.all([...users.map(createBooksPerUser)]);
}

module.exports = createBooks;
